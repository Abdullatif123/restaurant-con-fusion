import { Component, OnInit } from '@angular/core';
import { Dish } from 'src/shared/dish';
import { DishService } from '../services/dish.service';
import { Promotion } from 'src/shared/promotion';
import { PromotionService } from 'src/app/services/promotion.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  dish: Dish;
  promotion: Promotion;

  constructor(private dishservice: DishService,
    private promotionservice: PromotionService) { }

  ngOnInit() {
    this.dishservice.getFeaturedDish().subscribe(dishes=>this. dish = dishes)
    this.promotionservice.getFeaturedPromotion().subscribe(promotion=>this.promotion=promotion)
  }
}
